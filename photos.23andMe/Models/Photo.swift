//
//  Photo.swift
//  photos.23andMe
//
//  Created by edward lucero on 5/11/18.
//  Copyright © 2018 delirium. All rights reserved.
//

import Foundation
import ObjectMapper

class Photo: Mappable {
    var urlPhotoThumbnail : String?
    var urlPhotoLowRes : String?
    var urlPhotoHiRes : String?
    var id : String?
    var userLiked : Bool?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        urlPhotoThumbnail   <- map["images.thumbnail.url"]
        urlPhotoLowRes      <- map["images.low_resolution.url"]
        urlPhotoHiRes       <- map["images.standard_resolution.url"]
        id                  <- map["id"]
        userLiked           <- map["user_has_liked"]
    }
}
