//
//  PhotoApiResponse.swift
//  photos.23andMe
//
//  Created by edward lucero on 5/12/18.
//  Copyright © 2018 delirium. All rights reserved.
//

import Foundation
import ObjectMapper

class PhotoApiResponse: Mappable {
    var photos : [Photo]?
    
    required init?(map: Map) {

    }
    
    func mapping(map: Map) {
        photos <- map["data"]
    }
}
