//
//  Constants.swift
//  photos.23andMe
//
//  Created by edward lucero on 5/12/18.
//  Copyright © 2018 delirium. All rights reserved.
//

import Foundation

// APIs provided in coding excercise
let API_URL_LOGIN = "https://insta23prod.auth.us-west-2.amazoncognito.com/login?response_type=token&client_id=5khm2intordkd1jjr7rbborbfj&redirect_uri=https://www.23andme.com/"
let API_URL_LOGOUT = "https://insta23prod.auth.us-west-2.amazoncognito.com/logout?response_type=token&client_id=5khm2intordkd1jjr7rbborbfj&logout_uri=https://www.23andme.com/"
let API_URL_PHOTOS_RECENT = "https://kqlpe1bymk.execute-api.us-west-2.amazonaws.com/Prod/users/self/media/recent"


// TODO: Separate URL into components and assemble URL from endpoint
let API_URL_REDIRECT = "https://www.23andme.com/"
let CLIENT_ID = "5khm2intordkd1jjr7rbborbfj"
let API_URL_PHOTOS_LIKES_BASE = "https://kqlpe1bymk.execute-api.us-west-2.amazonaws.com/Prod/media/"
let API_URL_PHOTOS_LIKES_ENDPOINT = "/likes"
// TODO: Brittle. Review API and better extraction of Token
let API_URL_STRING_TRIM_START = "id_token="
let API_URL_STRING_TRIM_END = "&access_token="


// TODO: Get this value from login API response key
let TOKEN_EXPIRATION_TIME = 3600.0

let kToken23AndMe = "kToken23AndMe"
let kDateLastToken = "kDateLastToken"

enum constantsEnum {
    static let userLoggedInNotification = Notification.Name("com.delirium.photos.23andMe.userLoggedIn")
}

/*
// REFERENCE NOTES
// like by i cons from the Noun Project
// https://stackoverflow.com/questions/31320819/scale-uibutton-animation-swift
// https://stackoverflow.com/questions/28964346/swift-continuous-rotation-animation-not-so-continuous
// https://stackoverflow.com/questions/30480672/how-to-convert-a-json-string-to-a-dictionary
// https://www.raywenderlich.com/173576/ios-animation-tutorial-custom-view-controller-presentation-transitions-3
// https://www.sitepoint.com/improve-swift-closures-resul
*/
