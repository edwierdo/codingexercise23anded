//
//  APIManager.swift
//  photos.23andMe
//
//  Created by edward lucero on 5/12/18.
//  Copyright © 2018 delirium. All rights reserved.
//

import Foundation
import Alamofire

let UnDefineError = 000
let HTTPStatusCode_OK = 200

enum Result<T> {
    case Success(T)
    case Error(String, Int)
}

class APIManager {
    
    typealias APIManagerFetchDataCompletion = (Result<[Photo]>) -> Void
    typealias APIManagerLikeCompletion = (Result<Bool>) -> Void
    typealias APIManagerUnlikeCompletion = (Result<Bool>) -> Void
    typealias APIManagerLogOut = (_ success:Bool) -> Void
    
    static let shared = APIManager()
    
    private init() {
    }
    
    func logOut (completion: APIManagerLogOut? = nil) {
        Alamofire.request(API_URL_LOGOUT, method: .get).responseString(completionHandler: { response in
            switch (response.result){
            case .success:
                UserDefaults.standard.set(nil, forKey: kToken23AndMe)
                completion!(true)
            case .failure:
                completion!(false)
            }            
        })
    }
    
    func fetchPhotos(completion: @escaping APIManagerFetchDataCompletion) {
        guard let headers = createStandardURLHeader() else {
            print("Unable to create header. Possibly missing token Unable to create header. Possibly missing token")
            return
        }
        Alamofire.request(API_URL_PHOTOS_RECENT, method: .get, headers:headers).responseString(completionHandler: { response in
            switch (response.result){
            case .success(let responseString):
                if let status = response.response?.statusCode {
                    switch(status){
                    case HTTPStatusCode_OK:
                        if let photoResponse = PhotoApiResponse(JSONString:"\(responseString)") {
                            if let ckrecord: [Photo] = photoResponse.photos {
                                return completion(.Success(ckrecord))
                            }
                        }
                    default:
                        if let responseDictionary = Utils.convertToDictionary(text: response.result.value!) {
                            if let msg = responseDictionary["message"] as? String {
                                return completion(.Error(msg,status))
                            }
                        }
                    }
                }
                completion(.Error("Undefined Error",UnDefineError))
            case .failure(let error):
                completion(.Error(error.localizedDescription,UnDefineError))
            }
        })
    }
    
    func likePhotos(_ mediaId:String, completion: @escaping APIManagerLikeCompletion) {
        guard let headers = createStandardURLHeader() else {return}
        Alamofire.request(API_URL_PHOTOS_LIKES_BASE+mediaId+API_URL_PHOTOS_LIKES_ENDPOINT, method:.post,encoding: JSONEncoding.default, headers:headers).responseJSON { response in
            switch (response.result){
            case .success(let responseString):
                if let status = response.response?.statusCode {
                    switch(status){
                    case HTTPStatusCode_OK:
                        return completion(.Success(true))
                    default:
                        if let object = responseString as? [String: Any] {
                            if let msg = object["message"] as? String {
                                return completion(.Error(msg,status))
                            }
                        }
                    }
                }
                completion(.Error("",UnDefineError))
            case .failure(let error):
                return completion(.Error(error.localizedDescription,UnDefineError))
            }
        }
    }
    
    func unlikePhotos(_ mediaId:String, completion: @escaping APIManagerUnlikeCompletion) {
        guard let headers = createStandardURLHeader() else {return}
        Alamofire.request(API_URL_PHOTOS_LIKES_BASE+mediaId+API_URL_PHOTOS_LIKES_ENDPOINT, method:.delete,encoding: JSONEncoding.default, headers:headers).responseJSON { response in
            print(response)
            switch (response.result){
            case .success(let responseString):
                if let status = response.response?.statusCode {
                    switch(status){
                    case HTTPStatusCode_OK:
                        return completion(.Success(true))
                    default:
                        if let object = responseString as? [String: Any] {
                            if let msg = object["message"] as? String {
                                return completion(.Error(msg,status))
                            }
                        }
                    }
                }
                completion(.Error("",UnDefineError))
            case .failure(let error):
                return completion(.Error(error.localizedDescription,UnDefineError))
            }
        }
    }
}

extension APIManager {
    
    // Add Key/Values to supress API Server response warnings
    func createStandardURLHeader() -> Dictionary <String,String>? {
        if let tokenFromDefaults = UserDefaults.standard.object(forKey: kToken23AndMe) as? String {         // TODO save token in keychain
            return [
                "Authorization": tokenFromDefaults,
                "Signature": "",
                "Credential": "",
                "SignedHeaders": "",
                "Date": ""
            ]
        }
        return nil
    }
}
