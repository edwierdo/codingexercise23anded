//
//  PhotoCollageCollectionViewController.swift
//  photosLiker
//
//  Created by edward lucero on 5/11/18.
//  Copyright © 2018 delirium. All rights reserved.
//

import UIKit
import SDWebImage

private let reuseIdentifier = "PhotoCell"

class PhotoCollageCollectionViewController: UICollectionViewController, UICollectionViewDelegateFlowLayout{

    var hasLoggedIn : Bool = false
    var isAnimatingActivityIndicator : Bool = false
    var dataArray = [Photo]()
    @IBOutlet weak var imageViewActivityIndicator: UIImageView!
    
    // segue animation
    let transition = ZoomInAnimator()
    var selectedImage: UIImageView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(PhotoCollageCollectionViewController.fetchData(_:)), name: constantsEnum.userLoggedInNotification, object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if !hasExistingToken() {
            self.showSplashView(false)
        }
    }
    
    @IBAction func actionOptions(_ sender: Any) {
        showActionSheetMenu()
    }
    
    @objc func fetchData(_ note: Notification) {
        fetchPhotoData()
    }

    // MARK: UICollectionViewDataSource

    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }

    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dataArray.count
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as? PhotoCollectionViewCell else { fatalError("Unable to dequeue PhotoCollectionViewCell") }
        if dataArray[indexPath.row].urlPhotoThumbnail != nil {
            if let urlString = dataArray[indexPath.row].urlPhotoThumbnail {
                cell.imageViewPhoto.sd_setImage(with:URL(string: urlString), placeholderImage: nil)
            }
        }
        if let flagUserLiked = dataArray[indexPath.row].userLiked {
            if flagUserLiked {
                cell.userHasLiked = true
                cell.btnLike.setImage(UIImage(named: "heartRed"), for: .normal)
            } else {
                cell.userHasLiked = false
                cell.btnLike.setImage(UIImage(named: "heart"), for: .normal)
            }
        }
        cell.delegate = self
        return cell
    }

    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let cell = collectionView.cellForItem(at: indexPath) as? PhotoCollectionViewCell else { return }
        let vc:PhotoDetailViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PhotoDetailView") as! PhotoDetailViewController
        if let photoImage = cell.imageViewPhoto.image {
            vc.previewImage = photoImage
        }
        selectedImage = cell.imageViewPhoto
        if dataArray[indexPath.item].urlPhotoHiRes != nil {
            if let urlString = dataArray[indexPath.item].urlPhotoHiRes {
                vc.photoHiResURL = urlString
            }
        }
        vc.transitioningDelegate = self
        present(vc, animated: true, completion: nil)
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.bounds.size.width/2, height: collectionView.bounds.size.width/2)
    }
    
}

// MARK: - Delegate for CollectionViewCell
extension PhotoCollageCollectionViewController:PhotoCollectionViewCellDelegate {
    
    func photoCellDidTapLike(_ sender: PhotoCollectionViewCell) {
        guard let tappedIndexPath = collectionView?.indexPath(for: sender) else { return }
        guard let mediaID = dataArray[tappedIndexPath.item].id else { return }
        if let flagUserLiked = dataArray[tappedIndexPath.item].userLiked {
            likePhoto(mediaID, like: flagUserLiked)
        }
    }
    
}

// MARK: - Validation
extension PhotoCollageCollectionViewController {
    
    func hasExistingToken() -> Bool {
        if UserDefaults.standard.object(forKey: kToken23AndMe) != nil {
            return true
        }
        return false
    }
    
}

// MARK: - API NETWORK
extension PhotoCollageCollectionViewController {

    func fetchPhotoData() {
        self.startAnimatingActivityIndicator()
        APIManager.shared.fetchPhotos() { result in
            self.stopAnimatingActivityIndicator()
            switch result {
            case .Success(let photoArray):
                self.dataArray = photoArray
                self.collectionView?.reloadData()
            case .Error(let msg, let errorCode):
                Utils.showAlert(title: "Error \(errorCode)", message: msg, in: self)
            }
        }
    }
    
    func likePhoto(_ mediaID:String, like:Bool) {
        self.startAnimatingActivityIndicator()
        //TODO: handle response uniquely or refactor
        if !like {
            APIManager.shared.likePhotos(mediaID) { result in
                switch result {
                case .Success( _):
                    self.fetchPhotoData()
                case .Error(let msg, let errorCode):
                    self.stopAnimatingActivityIndicator()
                    Utils.showAlert(title: "Error \(errorCode)", message: msg, in: self)
                }
            }
        } else {
            APIManager.shared.unlikePhotos(mediaID) { result in
                switch result {
                case .Success( _):
                    self.fetchPhotoData()
                case .Error(let msg, let errorCode):
                    self.stopAnimatingActivityIndicator()
                    Utils.showAlert(title: "Error \(errorCode)", message: msg, in: self)
                }
            }
        }
    }
    
    func logout() {
        APIManager.shared.logOut() { success in
            if success {self.showSplashView(true)}
        }
        //TODO: Handle logout failure
    }
    
    
}

// MARK: - UI
extension PhotoCollageCollectionViewController {
    
    func startAnimatingActivityIndicator() {
        if !isAnimatingActivityIndicator {
            isAnimatingActivityIndicator = true
            imageViewActivityIndicator.rotateToInfinity()
        }
    }
    
    func stopAnimatingActivityIndicator() {
        isAnimatingActivityIndicator = false
        imageViewActivityIndicator.layer.removeAllAnimations()
    }
    
    func showSplashView(_ animated: Bool) {
        let nc:UINavigationController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "splashNavigationController") as! UINavigationController
        navigationController?.present(nc, animated: animated) {
        }
    }
    
    func showActionSheetMenu() {
        let alert = UIAlertController(title: "Menu", message:nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Refresh Data", style: .default , handler:{ (UIAlertAction)in
            self.fetchPhotoData()
        }))
        alert.addAction(UIAlertAction(title: "Log Out", style: .default , handler:{ (UIAlertAction)in
            self.logout()
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler:{ (UIAlertAction)in }))
        self.present(alert, animated: true, completion: {})
    }
    
    // Hide/unhide navigation bar when scrolled to maximize content viewing
    override func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        if scrollView.panGestureRecognizer.translation(in: scrollView).y < 0 {
            UIView.animate(withDuration: 3.0, delay: 0, options: UIViewAnimationOptions(), animations: {
                self.navigationController?.setNavigationBarHidden(true, animated: true)
            }, completion: nil)
        } else {
            UIView.animate(withDuration: 3.0, delay: 0, options: UIViewAnimationOptions(), animations: {
                self.navigationController?.setNavigationBarHidden(false, animated: true)
            }, completion: nil)
        }
    }
}

extension PhotoCollageCollectionViewController: UIViewControllerTransitioningDelegate {
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        transition.originFrame =
            selectedImage!.superview!.convert(selectedImage!.frame, to: nil)
        return transition
    }
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        //TODO: Animate in both directions
        return nil
    }
}
