//
//  SplashViewController.swift
//  photos.23andMe
//
//  Created by edward lucero on 5/12/18.
//  Copyright © 2018 delirium. All rights reserved.
//

import UIKit

class SplashViewController: UIViewController {

    @IBOutlet weak var btnSignIn: UIButton!
    @IBOutlet weak var labelStatus: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        btnSignIn.layer.cornerRadius = btnSignIn.frame.height/2
        btnSignIn.layer.borderColor = UIColor.lightGray.cgColor
        btnSignIn.layer.borderWidth = 1.0
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
    }

    @IBAction func tapLogin(_ sender: Any) {
        let vc:LoginViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "loginview") as! LoginViewController
        self.show(vc, sender: self)
    }
}
