//
//  LogInViewController.swift
//  photosLiker
//
//  Created by edward lucero on 5/11/18.
//  Copyright © 2018 delirium. All rights reserved.
//

import UIKit
import WebKit

let URL_RESPONSE_URI = "www.23andme.com"

class LoginViewController: UIViewController, WKNavigationDelegate {
    
    @IBOutlet weak var webView: WKWebView!
    @IBOutlet weak var imageViewActivityIndicator: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        webView.navigationDelegate = self
        if let url = URL(string: API_URL_LOGIN) {
            imageViewActivityIndicator.rotateToInfinity()
            webView.load(URLRequest(url: url))
        }
    }
    
    @IBAction func actionCancel(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void)
    {
        if let authURL:URL = navigationAction.request.url
        {
            let urlComponents = URLComponents(string: authURL.absoluteString)
            if let hostString:String = urlComponents?.host{
                if hostString == URL_RESPONSE_URI {
                    if let fragmentString:String = urlComponents?.fragment {
                        if let id_token = fragmentString.trim(start: API_URL_STRING_TRIM_START, end: API_URL_STRING_TRIM_END) {
                            storeTokenInKeychain(token: id_token)
                        }
                    }
                    decisionHandler(WKNavigationActionPolicy.cancel)
                    self.postLoginNotification()
                    self.navigationController?.dismiss(animated: true, completion: {
                    })
                    return
                }
            }
        }
        decisionHandler(WKNavigationActionPolicy.allow)
    }
    
    func webView(_ webView: WKWebView,
                 didFinish navigation: WKNavigation!) {
        imageViewActivityIndicator.layer.removeAllAnimations()
    }
    
    func storeTokenInKeychain(token: String) {
        //TODO: Use keychain enable secure storage of token
        UserDefaults.standard.set(token, forKey: kToken23AndMe)
        UserDefaults.standard.set(Date(), forKey: kDateLastToken)
    }
    
    func postLoginNotification() {
        let note = Notification(name: constantsEnum.userLoggedInNotification)
        NotificationCenter.default.post(note)
    }
}


