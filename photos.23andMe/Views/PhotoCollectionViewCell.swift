//
//  PhotoCollectionViewCell.swift
//  photos.23andMe
//
//  Created by edward lucero on 5/12/18.
//  Copyright © 2018 delirium. All rights reserved.
//

import UIKit

protocol PhotoCollectionViewCellDelegate : class {
    func photoCellDidTapLike(_ sender: PhotoCollectionViewCell)
}

class PhotoCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imageViewPhoto: UIImageView!
    @IBOutlet weak var btnLike: UIButton!
    var userHasLiked : Bool = false
    weak var delegate: PhotoCollectionViewCellDelegate?
    
    
    @IBAction func tapLike(_ sender: Any) {
        print("tapLike")
        if !userHasLiked {
            animatePulseImage()
            userHasLiked = true
        } else {
            userHasLiked = false
        }
        delegate?.photoCellDidTapLike(self)
    }
    
    
    func animatePulseImage() {
        self.imageViewPhoto.transform = CGAffineTransform(scaleX: 0.9, y: 0.9)
        UIView.animate(withDuration: 1.0,
                       delay: 0,
                       usingSpringWithDamping: CGFloat(0.20),
                       initialSpringVelocity: CGFloat(4.0),
                       options: UIViewAnimationOptions.allowUserInteraction,
                       animations: {
                        self.imageViewPhoto.transform = CGAffineTransform.identity
        },completion: { Void in()})
    }
    
    
}
