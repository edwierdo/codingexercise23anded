//
//  PhotoDetailViewController.swift
//  photos.23andMe
//
//  Created by edward lucero on 5/13/18.
//  Copyright © 2018 delirium. All rights reserved.
//

import UIKit
import SDWebImage

class PhotoDetailViewController: UIViewController, UIViewControllerTransitioningDelegate {

    @IBOutlet weak var imageViewPhoto: UIImageView!
    var previewImage: UIImage!
    var photoHiResURL: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(actionDismiss(_:))))
        imageViewPhoto.sd_setImage(with:URL(string: photoHiResURL), placeholderImage: previewImage)
    }

    @objc func actionDismiss(_ tap: UITapGestureRecognizer) {
        presentingViewController?.dismiss(animated: true, completion: nil)
    }

}
