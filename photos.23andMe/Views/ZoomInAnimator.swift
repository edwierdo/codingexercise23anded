//
//  ZoomInAnimator.swift
//  photos.23andMe
//
//  Created by edward lucero on 5/13/18.
//  Copyright © 2018 delirium. All rights reserved.
//

import UIKit

class ZoomInAnimator: NSObject, UIViewControllerAnimatedTransitioning {

    let duration = 1.0
    var originFrame = CGRect.zero
    
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return duration
    }
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        // only handling forward transition
        let containerView = transitionContext.containerView
        let toView = transitionContext.view(forKey: .to)!
        let photoDetailView = toView
        let initialFrame = originFrame
        let finalFrame = photoDetailView.frame
        let xScale = initialFrame.width / finalFrame.width
        let yScale = initialFrame.height / finalFrame.height
        let scaleTransform = CGAffineTransform(scaleX: xScale, y: yScale)
        photoDetailView.transform = scaleTransform
        photoDetailView.center = CGPoint(
            x: initialFrame.midX,
            y: initialFrame.midY)
        photoDetailView.clipsToBounds = true
        containerView.addSubview(toView)
        containerView.bringSubview(toFront: photoDetailView)
        UIView.animate(withDuration: duration, delay:0.0,
                       usingSpringWithDamping: 0.4, initialSpringVelocity: 0.0,
                       animations: {
                        photoDetailView.transform = CGAffineTransform.identity
                        photoDetailView.center = CGPoint(x: finalFrame.midX, y: finalFrame.midY)
        }, completion: { _ in
                        transitionContext.completeTransition(true)
        }
        )
    }
    
}
